package com.dak.perbarindo.interfaces;


import com.dak.perbarindo.models.menu.ValueMenu;
import com.dak.perbarindo.models.user.ValueUser;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;


public interface ApiService {


    @Multipart
    @POST("login")
    Call<ValueUser> login(@Part("username") String username, @Part("password") String password);

    @Multipart
    @POST("member_checkin")
    Call<String> sendCheckin(@Part("regcode") String regcode, @Part("event_member_id") String event_member_id,@Part("userid") String userid );


    @GET("starterkit")
    Call<String>getStarterKitList();

//    @Multipart
//    @POST("submit_staterkit")
//    Call<String> sendStarterKit(@Part("regcode") String regcode, @Part("event_member_id") String event_member_id, @Part("userid") String userid, @PartMap Map<String, String> startter_kits);

    @Multipart
    @POST("submit_staterkit")
    Call<String> sendStarterKit(@Part("regcode") String regcode,
                                @Part("event_member_id") String event_member_id,
                                @Part("userid") String userid,
                                @Part ("starterkit_id[0]") String id);



    @Multipart
    @POST("submit_document")
    Call<String> sendDocument(@Part("regcode") String regcode, @Part("event_member_id") String event_member_id, @Part("userid") String userid, @Part("doc_type") String doc_type);

    @GET("get_cover")
    Call<String>getCover();




}
