package com.dak.perbarindo.models.menu;

import com.dak.perbarindo.models.BaseResponse;

import java.util.ArrayList;

public class ValueMenu extends BaseResponse {

    private ArrayList<Menu> data;

    public ValueMenu(ArrayList<Menu> data) {
        this.data = data;
    }

    public ArrayList<Menu> getData() {
        return data;
    }
}
