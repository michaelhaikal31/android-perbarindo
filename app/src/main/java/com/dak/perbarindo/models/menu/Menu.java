package com.dak.perbarindo.models.menu;

import com.dak.perbarindo.models.BaseResponse;

import java.io.Serializable;

public class Menu extends BaseResponse implements Serializable {
    private String id;
    private String name;
    private String status;
    private String check_in_menu_id;
    private String sequence;
    private Boolean selected = Boolean.valueOf(false);

    public Menu() {
    }

    public Menu(String id, String name, String status, String check_in_menu_id, String sequence) {
        this.id = id;
        this.name = name;
        this.status = status;
        this.check_in_menu_id = check_in_menu_id;
        this.sequence = sequence;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getCheck_in_menu_id() {
        return check_in_menu_id;
    }

    public String getSequence() {
        return sequence;
    }

    public Boolean getSelected() {
        return this.selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", check_in_menu_id='" + check_in_menu_id + '\'' +
                ", sequence='" + sequence + '\'' +
                ", selected=" + selected +
                '}';
    }
}
