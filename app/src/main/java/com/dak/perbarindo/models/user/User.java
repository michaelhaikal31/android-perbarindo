package com.dak.perbarindo.models.user;

public class User {
    private String id;
    private String email;
    private String username;
    private String first_name;
    private String last_name;
    private String picture;

    public User(String id, String email, String username, String first_name, String last_name, String picture) {
        this.id = id;
        this.email = email;
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.picture = picture;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPicture() {
        return picture;
    }

}
