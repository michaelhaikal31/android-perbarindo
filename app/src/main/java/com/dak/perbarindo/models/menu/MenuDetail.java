package com.dak.perbarindo.models.menu;

import java.io.Serializable;

public class MenuDetail implements Serializable {
    private String check_in_menu_id;
    private String menuId;
    private String name;
    private Boolean selected = Boolean.valueOf(false);
    private String sequence;
    private String status;

    public String getMenuId() {
        return this.menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSequence() {
        return this.sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getCheck_in_menu_id() {
        return this.check_in_menu_id;
    }

    public void setCheck_in_menu_id(String check_in_menu_id) {
        this.check_in_menu_id = check_in_menu_id;
    }

    public Boolean getSelected() {
        return this.selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }
}
