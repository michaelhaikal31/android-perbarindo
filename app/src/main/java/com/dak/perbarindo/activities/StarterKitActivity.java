package com.dak.perbarindo.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.dak.perbarindo.R;
import com.dak.perbarindo.interfaces.ApiService;
import com.dak.perbarindo.models.menu.Menu;
import com.dak.perbarindo.services.RetrofitService;
import com.dak.perbarindo.utility.Constant;
import com.dak.perbarindo.utility.DataStore;
import com.dak.perbarindo.adapters.MenuDetailListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StarterKitActivity extends AppCompatActivity  {
    private Button button;
    private ArrayList<Menu> menu;
    private String link;
    private ListView listView;
    private ProgressDialog progressDialog;
    private MenuDetailListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Member Starterkit");
        setContentView(R.layout.activity_starter_kit);
        listView = findViewById(R.id.list_starterkit);
        button = findViewById(R.id.scan);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNothingSelected()){
                    Intent intent = new Intent(StarterKitActivity.this,Scanner.class);
                    intent.putExtra("menu",2);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("list_starter_kit",menu);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(StarterKitActivity.this,"Silahkan pilih salah satu starterkit",Toast.LENGTH_SHORT).show();
                }

            }
        });
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait !");
        progressDialog.show();
        getlist();
    }

    private void getlist(){
        DataStore dataStore = new DataStore();
        ApiService apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+Constant.URL_MENU_DETAIL).create(ApiService.class);
        Call<String> call = apiInterface.getStarterKitList();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    System.out.println("asd response "+response.body());
                    JSONObject jsonObject = new JSONObject(response.body());
                    if (jsonObject.getBoolean("status")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        JSONObject responMenu;
                        menu = new ArrayList<>();
                        for(int i=0;i<jsonArray.length();i++){
                            responMenu = new JSONObject(jsonArray.get(i).toString());
                            menu.add(new Menu(responMenu.getString("id"),
                                    responMenu.getString("name"),
                                    responMenu.getString("status"),
                                    responMenu.getString("check_in_menu_id"),
                                    responMenu.getString("sequence")
                                    ));
                        }
                        System.out.println("asd tostring "+menu.toString());
                        adapter = new MenuDetailListAdapter(StarterKitActivity.this, menu);
                        listView.setAdapter(adapter);
                    } else {

                    }
                    progressDialog.dismiss();
                }catch (JSONException e){
                    System.out.println("asd json exception "+e.toString());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd failure "+t.getMessage());
                progressDialog.dismiss();
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;

    }

    private boolean isNothingSelected(){
        for(int i=0;i<menu.size();i++){
            if(menu.get(i).getSelected()){
                return true;
            }
        }
        return false;

    }


}
