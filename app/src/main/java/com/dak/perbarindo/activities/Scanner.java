package com.dak.perbarindo.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dak.perbarindo.R;
import com.dak.perbarindo.interfaces.ApiService;
import com.dak.perbarindo.models.menu.Menu;
import com.dak.perbarindo.services.RetrofitService;
import com.dak.perbarindo.utility.Constant;
import com.dak.perbarindo.utility.DataStore;
import com.google.gson.Gson;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.Inflater;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Scanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private static final int REQUEST_CAMERA = 1001;
    private String id_login;
    private String link;
    private ZXingScannerView mScannerView;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private int menu;
    private ArrayList<Menu> listStarterKit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        this.sharedPreferences = getSharedPreferences(Constant.login_details, 0);
        this.id_login = this.sharedPreferences.getString(Constant.id_user, "");
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            menu = getIntent().getExtras().getInt("menu");
            if(menu==2){

//                listStarterKit =(ArrayList)getIntent().getExtras().getSerializable("list_starter_kit");
            }
        }

        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);

    }

    @Override
    public void handleResult(Result rawResult) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait !");
        progressDialog.show();
        System.out.println("asd scan result "+rawResult.getText());
        sendData(rawResult.getText());
//        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
//        } else {
//            //deprecated in API 26
//            v.vibrate(500);
//        }
//        System.out.println("asd "+ rawResult.getText()); // Prints scan results
//        System.out.println("asd "+ rawResult.getBarcodeFormat().toString());
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Scan Berhasil");
//        builder.setMessage(rawResult.getText());
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//           mScannerView.resumeCameraPreview(Scanner.this);
//            }
//        });
//        AlertDialog alertDialog = builder.create();
//        alertDialog.show();


    }

    private void sendData(String scanResult){
        String result[] = scanResult.split("-");
        DataStore dataStore = new DataStore();
        ApiService apiInterface;
        Call<String> call=null;
        switch (menu){
            case 1:
                 System.out.println("asd switch 1");
                 apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+Constant.URL_CHECK_IN).create(ApiService.class);
                 call = apiInterface.sendCheckin(result[0]+"-"+result[1],result[2],dataStore.getIdUser(this));
                 System.out.println("asd datakiriman "+result[0]+"-"+result[1]+"|"+result[2]+"|"+dataStore.getIdUser(this));
                break;
            case 2:
                System.out.println("asd switch 2");
                apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+Constant.URL_SUBMIT_STATERKIT).create(ApiService.class);
//                Map<String, String> params = new HashMap<>();
//                int temp=0;
//                for(int i=0;i<listStarterKit.size();i++){
//                    if(listStarterKit.get(i).getSelected()){
//                        params.put("starterkit_id["+temp+"]",listStarterKit.get(i).getId());
//                        temp++;
//                    }
//                }
                System.out.println("asd id user "+dataStore.getIdUser(this));
                call = apiInterface.sendStarterKit(result[0]+"-"+result[1],result[2],dataStore.getIdUser(this),"5");
                break;
            case 3:
                System.out.println("asd switch 3");
                apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+Constant.URL_CHECK_IN).create(ApiService.class);
                call = apiInterface.sendDocument(result[0]+"-"+result[1],result[2],dataStore.getIdUser(this),"CERTIFICATE");
                break;
            case 4:
                System.out.println("asd switch 4");
                apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+Constant.URL_CHECK_IN).create(ApiService.class);
                call = apiInterface.sendDocument(result[0]+"-"+result[1],result[2],dataStore.getIdUser(this),"RECEIPT");
                break;
        }

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    System.out.println("asd response " + response.body().toString());
                    System.out.println("asd response 2 " + response.message());
                        JSONObject jsonObject = new JSONObject(response.body());
                        if (jsonObject.getBoolean("status")) {
                            showDialogResult(jsonObject.getJSONObject("data").toString());
                        } else {
                            showDialog("Respon", jsonObject.getString("error"));
                        }

                    //mScannerView.resumeCameraPreview(Scanner.this);

                }catch (Exception e){
                    showDialog("Failed","Internal server error");
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd failure "+t.getMessage());
                progressDialog.dismiss();
                showDialog("Failed", "Connection problem with server !");
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    private void showDialogResult(String response){

        TextView txtNama,txtPic,txtBpr,txtDpd,txtEvent,txtNote,txtHeaderNote;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_scan_result, null);
        StringBuilder scanned = new StringBuilder();
        StringBuilder scanned2 = new StringBuilder();
        txtNama = dialogView.findViewById(R.id.txtNamaDialog);
        txtPic = dialogView.findViewById(R.id.txtPicDialog);
        txtBpr = dialogView.findViewById(R.id.txtBprDialog);
        txtDpd = dialogView.findViewById(R.id.txtDpdDialog);
        txtEvent = dialogView.findViewById(R.id.txtEventDialog);
        txtNote = dialogView.findViewById(R.id.txtNoteDialog);
        txtHeaderNote = dialogView.findViewById(R.id.txtHeaderNote);
        try {
            JSONObject data = new JSONObject(response);
            txtNama.setText(data.getString("name"));
            txtPic.setText(data.getString("pic"));
            txtBpr.setText(data.getString("bpr"));
            txtDpd.setText(data.getString("dpd"));
            txtEvent.setText(data.getString("event"));
            switch (menu){
                case 2:
                    if (data.has("starterkit_scanned")) {
                        JSONArray data1 = new JSONArray(data.getString("starterkit_scanned"));
                        for (int y = 0; y < data1.length(); y++) {
                            JSONObject jsonObject2 = data1.getJSONObject(y);
                            if (y + 1 == data1.length()) {
                                scanned.append(jsonObject2.getString("name"));
                            } else {
                                scanned.append(jsonObject2.getString("name") + ", ");
                            }
                        }
                    }
                    if (data.has("starterkit_already_scanned")) {
                        JSONArray data2 = new JSONArray(data.getString("starterkit_already_scanned"));
                        for (int z = 0; z < data2.length(); z++) {
                            JSONObject jsonObject = data2.getJSONObject(z);
                            if (z + 1 == data2.length()) {
                                scanned2.append(jsonObject.getString("name"));
                            } else {
                                scanned2.append(jsonObject.getString("name") + ", ");
                            }
                        }
                    }

                    break;
                default:
                    txtHeaderNote.setVisibility(View.GONE);
                    txtNote.setVisibility(View.GONE);
                    break;

            }

            if(scanned.length()!=0){
                txtHeaderNote.setText("Scanned");
                txtNote.setText(scanned);
            }else if(scanned2.length()!=0){
                txtHeaderNote.setText("Already Scanned");
                txtNote.setText(scanned2);
            }

        builder.setView(dialogView);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //mScannerView.resumeCameraPreview(Scanner.this);
                finish();
                return;
            }
        });
        builder.create();
        progressDialog.dismiss();
        builder.show();
        }catch (JSONException e){
            progressDialog.dismiss();
            showDialog("Error","Error when converting server response, "+e.getMessage());
        }

    }


    private void showDialog(String title,String message){
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                mScannerView.resumeCameraPreview(Scanner.this);
                finish();

            }
        });
        builder.create();
        progressDialog.dismiss();
        builder.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
                break;
        }
        return true;
    }
}
