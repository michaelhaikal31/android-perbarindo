package com.dak.perbarindo.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dak.perbarindo.R;
import com.dak.perbarindo.adapters.MenuDetailListAdapter;
import com.dak.perbarindo.interfaces.ApiService;
import com.dak.perbarindo.models.menu.Menu;
import com.dak.perbarindo.services.RetrofitService;
import com.dak.perbarindo.utility.Constant;
import com.dak.perbarindo.utility.DataStore;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    TextView txtUsername;
    TextView txtEmail;
    ImageView profil_picture;
    DataStore dataStore;
    //GridLayout mainGrid;
    ImageView cover;
    LinearLayout memberCheckin,setStarterKit,ambilKwitansi,ambilSertifikat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        requestPermission();
        memberCheckin = findViewById(R.id.linearLayout);
        setStarterKit = findViewById(R.id.linearLayout2);
        ambilKwitansi = findViewById(R.id.linearLayout3);
        ambilSertifikat = findViewById(R.id.linearLayout4);

        memberCheckin.setOnClickListener(this);
        setStarterKit.setOnClickListener(this);
        ambilSertifikat.setOnClickListener(this);
        ambilKwitansi.setOnClickListener(this);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView =  navigationView.getHeaderView(0);
        txtUsername = hView.findViewById(R.id.txtUserNameDrawer);
        txtEmail = hView.findViewById(R.id.txtEmailDrawer);
        profil_picture = hView.findViewById(R.id.imageViewDrawer);
        cover = findViewById(R.id.cover);
        dataStore = new DataStore();
        txtUsername.setText(dataStore.getUserName(this));
        System.out.println("asd fs"+dataStore.getEmail(this));
        txtEmail.setText(dataStore.getEmail(this));
        Glide.with(this)
                .load(dataStore.getPicture(this).toString())
                .into(profil_picture);
        //mainGrid = (GridLayout) findViewById(R.id.mainGrid);

        //setSingleEvent(mainGrid);
    }

    private void setSingleEvent(GridLayout mainGrid) {

        for (int i = 0; i < mainGrid.getChildCount(); i++) {
            CardView cardView = (CardView) mainGrid.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (finalI) {
                        case 0 :
                           Intent intent = new Intent(MainActivity.this,Scanner.class);
                           intent.putExtra("menu",1);
                           startActivity(intent);
                            break;
                        case 1:
                            Intent intent2 = new Intent(MainActivity.this,Scanner.class);
                            intent2.putExtra("menu",2);
                            startActivity(intent2);
                            break;

                        case 2 :
                            Intent intent3 = new Intent(MainActivity.this,Scanner.class);
                            intent3.putExtra("menu",3);
                            startActivity(intent3);
                            break;
                        case 3:
                            Intent intent4 = new Intent(MainActivity.this,Scanner.class);
                            intent4.putExtra("menu",4);
                            startActivity(intent4);
                            break;
                    }

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_setting) {
        //Intent intent = new Intent(MainActivity.this,Scanner.class);
        Intent intent = new Intent(MainActivity.this,HostnameActivity.class);
        intent.putExtra("menu",2);
        startActivity(intent);
        } else if (id == R.id.nav_exit) {
//            dataStore.setStatus(this,false);
//            dataStore.setHost(this,"");
            this.finishAffinity();
        }else if(id==R.id.nav_logout){
            dataStore.setStatus(this,false);
            Intent intent=new Intent(MainActivity.this,LoginActivity.class);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(dataStore.getCover(MainActivity.this).equalsIgnoreCase("")) {
            System.out.println("asd 1");
            loadCover();
        }else{
            System.out.println("asd 2 "+dataStore.getCover(this));
            Glide.with(MainActivity.this)
                    .load(dataStore.getCover(MainActivity.this).toString())
                    .into(cover);

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataStore.setCover(MainActivity.this,"");
    }



    private void loadCover(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Initializing...");
        progressDialog.show();
        ApiService apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+ Constant.URL_GET_COVER).create(ApiService.class);
        Call<String> call = apiInterface.getCover();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    System.out.println("asd response "+response.body());
                    JSONObject jsonObject = new JSONObject(response.body());
                    if (jsonObject.getBoolean("status")) {
                        dataStore.setCover(MainActivity.this,jsonObject.getString("cover").toString());
                        Glide.with(MainActivity.this)
                                .load(dataStore.getCover(MainActivity.this).toString())
                                .into(cover);
                    }
                    progressDialog.dismiss();
                }catch (JSONException e){
                    System.out.println("asd json exception "+e.toString());
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd failure "+t.getMessage());
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.linearLayout :

                Intent intent = new Intent(MainActivity.this,Scanner.class);
                intent.putExtra("menu",1);
                startActivity(intent);
                break;
            case R.id.linearLayout2:
                Intent intent2 = new Intent(MainActivity.this,Scanner.class);
                intent2.putExtra("menu",2);
                startActivity(intent2);
                break;

            case R.id.linearLayout3:
                Intent intent3 = new Intent(MainActivity.this,Scanner.class);
                intent3.putExtra("menu",3);
                startActivity(intent3);
                break;
            case R.id.linearLayout4:
                Intent intent4 = new Intent(MainActivity.this,Scanner.class);
                intent4.putExtra("menu",4);
                startActivity(intent4);
                break;
        }
    }

    private void requestPermission(){
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.INTERNET,
                        Manifest.permission.CAMERA
                ).withListener(new MultiplePermissionsListener() {
            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {/* ... */}
        }).check();
    }
}
