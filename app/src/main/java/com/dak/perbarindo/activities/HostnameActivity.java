package com.dak.perbarindo.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dak.perbarindo.R;
import com.dak.perbarindo.interfaces.ApiService;
import com.dak.perbarindo.services.RetrofitService;
import com.dak.perbarindo.utility.Constant;
import com.dak.perbarindo.utility.DataStore;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HostnameActivity extends AppCompatActivity {
    EditText txtHostName;
    Button btnSimpan;
    DataStore dataStore;
    private int menu;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setContentView(R.layout.activity_hostname);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            menu = getIntent().getExtras().getInt("menu");
            System.out.println("asd previous menu "+menu);
        }
        txtHostName = findViewById(R.id.txtHostname);
        btnSimpan = findViewById(R.id.btnSimpanHostname);
        dataStore = new DataStore();
        if(!dataStore.getHost(this).isEmpty()){
            txtHostName.setText(dataStore.getHost(this).replace("http://",""));
        }
        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isValidUrl("http://"+txtHostName.getText().toString().trim().toLowerCase())){

                   checkServer();


                }else{
                    shodDialog("Invalid url","Silahkan masukan url yang benar");
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;

    }

    private boolean isValidUrl(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url.toLowerCase());
        return m.matches();
    }

    private void shodDialog(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                return;
            }
        });
        builder.create();
        builder.show();
    }

    private void checkServer() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting..");
        progressDialog.show();
        ApiService apiInterface = RetrofitService.getRetrofit("http://"+txtHostName.getText().toString().trim().toLowerCase()+Constant.URL_MENU_DETAIL).create(ApiService.class);
        Call<String> call = apiInterface.getStarterKitList();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                System.out.println("asd "+response.body().toString());
                if(response.isSuccessful()){
                    dataStore.setHost(HostnameActivity.this,"http://"+txtHostName.getText().toString().trim().toLowerCase());
                    if(menu==1){
                        Intent intent = new Intent(HostnameActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(HostnameActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }else{
                    shodDialog("Failed","Tidak dapat terhubung ke "+"http://"+txtHostName.getText().toString().toLowerCase());
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("asd failure "+t.getMessage());
                shodDialog("Failed","Server tidak merespon, pastikan anda memasukan url yang benar ");
                progressDialog.dismiss();
            }
        });
    }


}
