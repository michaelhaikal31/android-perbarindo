package com.dak.perbarindo.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.dak.perbarindo.R;
import com.dak.perbarindo.models.user.ValueUser;
import com.dak.perbarindo.interfaces.ApiService;
import com.dak.perbarindo.services.RetrofitService;
import com.dak.perbarindo.utility.Constant;
import com.dak.perbarindo.utility.DataStore;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static SharedPreferences sharedPreferences;
    private ImageView imageView;
    private String link;
    private EditText password;
    private ProgressDialog progressDialog;
    private boolean status;
    private boolean toggle = false;
    private EditText username;
    private DataStore dataStore;
    private Button btnLogin;
    private ImageView btnSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataStore = new DataStore();
        status = dataStore.getStatus(this);
        System.out.println("Status Login : " + this.status);
        if (this.status) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            setContentView(R.layout.activity_login);
            getWindow().setBackgroundDrawableResource(R.drawable.bg_1);

        getSupportActionBar().hide();
        password =  findViewById(R.id.password);
        imageView = (ImageView) findViewById(R.id.toggle_view);
        username = findViewById(R.id.username);
        btnLogin = findViewById(R.id.btnLogin);
        btnSetting = findViewById(R.id.settingBtn);
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this,HostnameActivity.class);
                intent.putExtra("menu",1);
                startActivity(intent);
                finish();
            }
        });

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().addFlags(Integer.MIN_VALUE);
            getWindow().clearFlags(67108864);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(LoginActivity.this);
                progressDialog.setMessage("Please wait !");
                progressDialog.show();
                login();
            }
        });
        }

        if(dataStore.getHost(this).isEmpty()){
            System.out.println("");
            showDialog();
        }

    }

    public void onClickTogglePassword(View v) {
        if (this.toggle) {
            this.password.setTransformationMethod(PasswordTransformationMethod.getInstance());
            this.toggle = false;
        } else {
            this.password.setTransformationMethod(null);
            this.toggle = true;
        }
        this.password.setSelection(this.password.getText().length());
    }


    private void login(){
        ApiService apiInterface = RetrofitService.getRetrofit(dataStore.getHost(this)+Constant.URL_LOGIN).create(ApiService.class);
        Call<ValueUser> call = apiInterface.login(username.getText().toString(),password.getText().toString());
        call.enqueue(new Callback<ValueUser>() {
            @Override
            public void onResponse(Call<ValueUser> call, Response<ValueUser> response) {
                try {
                    System.out.println("asd " + response.body().getUser().getId());
                    if (response.body().isStatus()) {
                        dataStore.setIdUser(LoginActivity.this, response.body().getUser().getId());
                        dataStore.setEmail(LoginActivity.this, response.body().getUser().getEmail());
                        dataStore.setUserName(LoginActivity.this, response.body().getUser().getUsername());
                        dataStore.setFirstName(LoginActivity.this, response.body().getUser().getFirst_name());
                        dataStore.setLastName(LoginActivity.this, response.body().getUser().getLast_name());
                        dataStore.setPicture(LoginActivity.this, response.body().getUser().getPicture());
                        dataStore.setStatus(LoginActivity.this, true);
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        progressDialog.dismiss();
                        startActivity(intent);
                        finish();

                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this,"Cannot read server message",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e){
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this,"Cannot read server message",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ValueUser> call, Throwable t) {
                System.out.println("asd failure "+t.getMessage());
                Toast.makeText(LoginActivity.this,"Connection problem",Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void showDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Alamat host tidak ditemukan");
        builder.setMessage("Untuk dapat terhubung dengan server, anda perlu mengisi alamat host terlebih dahulu");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              Intent intent = new Intent(LoginActivity.this,HostnameActivity.class);
              intent.putExtra("menu",1);
              startActivity(intent);
              finish();
            }
        });
        builder.setNegativeButton("Keluar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }



}
