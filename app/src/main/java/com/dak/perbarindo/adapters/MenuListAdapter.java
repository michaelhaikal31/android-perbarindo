package com.dak.perbarindo.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.dak.perbarindo.R;
import com.dak.perbarindo.models.menu.Menu;

import java.util.ArrayList;

public class MenuListAdapter extends ArrayAdapter<Menu> {
    private Activity context;
    private ArrayList<Menu> listMenu;
    private OnChecked listener;

    public MenuListAdapter(Activity context, ArrayList<Menu> listMenu,OnChecked listener) {
        super(context, R.layout.list_detail, listMenu);
        this.context = context;
        this.listMenu = listMenu;
        this.listener = listener;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final View listViewItem = this.context.getLayoutInflater().inflate(R.layout.list_detail, null, true);
        final Menu menu = listMenu.get(position);
        TextView textView = listViewItem.findViewById(R.id.data_detail);
        CheckBox checkBox = listViewItem.findViewById(R.id.checkBox);
        textView.setText(menu.getName());
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.onOptionChecked(menu.getId(),isChecked);
            }
        });

        return listViewItem;
    }

    public interface OnChecked{
        void onOptionChecked(String id,Boolean isChecked);
    }
}
