package com.dak.perbarindo.adapters;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.dak.perbarindo.R;
import com.dak.perbarindo.models.menu.Menu;

import java.util.ArrayList;


public class MenuDetailListAdapter extends ArrayAdapter<Menu> {
    private Activity context;
    private ArrayList<Menu> listMenuDetails;

    private static class ViewHolder {
        public CheckBox checkBox;
        public TextView text;

        private ViewHolder() {
        }
    }

    public MenuDetailListAdapter(Activity context, ArrayList<Menu> listMenuDetails) {
        super(context, R.layout.list_detail, listMenuDetails);
        this.context = context;
        this.listMenuDetails = listMenuDetails;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = this.context.getLayoutInflater().inflate(R.layout.list_detail, null, true);
            holder = new ViewHolder();
            holder.text =  convertView.findViewById(R.id.data_detail);
            holder.checkBox =  convertView.findViewById(R.id.checkBox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.text.setText(( this.listMenuDetails.get(position)).getName());
        holder.checkBox.setTag(R.integer.btnplusview, convertView);
        holder.checkBox.setTag(Integer.valueOf(position));
        holder.checkBox.setChecked(((Menu) this.listMenuDetails.get(position)).getSelected().booleanValue());
        holder.checkBox.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                TextView tv =  ((View) holder.checkBox.getTag(R.integer.btnplusview)).findViewById(R.id.data_detail);
                Integer pos = (Integer) holder.checkBox.getTag();
                if (( MenuDetailListAdapter.this.listMenuDetails.get(pos.intValue())).getSelected().booleanValue()) {
                    ( MenuDetailListAdapter.this.listMenuDetails.get(pos.intValue())).setSelected(Boolean.valueOf(false));
                } else {
                    ( MenuDetailListAdapter.this.listMenuDetails.get(pos.intValue())).setSelected(Boolean.valueOf(true));
                }
            }
        });
        return convertView;
    }
}
