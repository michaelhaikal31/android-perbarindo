package com.dak.perbarindo.utility;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.view.inputmethod.InputMethodManager;
import org.json.JSONException;
import org.json.JSONObject;

public class Utility {

    /* renamed from: com.acs.perbarindo.utility.Utility$1 */
    static class C02241 implements OnClickListener {
        C02241() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    public static void hideVirtKeyboard(Activity activity) {
        IBinder iBinder;
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService("input_method");
        if (activity.getCurrentFocus() == null) {
            iBinder = null;
        } else {
            iBinder = activity.getCurrentFocus().getWindowToken();
        }
        inputManager.hideSoftInputFromWindow(iBinder, 2);
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static void popupNotification(Context context, String Title, String Message) {
        Builder alertDialog = new Builder(context);
        alertDialog.setTitle(Title);
        alertDialog.setMessage(Message).setCancelable(false);
        alertDialog.setPositiveButton("OK", new C02241()).show();
    }

    public static boolean isJson(String json) {
        try {
            JSONObject jSONObject = new JSONObject(json);
            return true;
        } catch (JSONException e) {
            return false;
        }
    }
}
