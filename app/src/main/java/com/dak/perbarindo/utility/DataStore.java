package com.dak.perbarindo.utility;

import android.content.Context;
import android.content.SharedPreferences;

public class DataStore  {

    public void setHost(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("host", value);
        editor.commit();

    }

    public  String getHost(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String nma = prefs.getString("host", "");
        return nma;
    }

    public void setStatus(Context context, Boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putBoolean("status", value);
        editor.commit();

    }

    public  Boolean getStatus(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        Boolean status = prefs.getBoolean("status", false);
        return status;
    }

    public void setIdUser(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("id_user", value);
        editor.commit();

    }

    public  String getIdUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("id_user", "");
        return status;
    }

    public void setEmail(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("email_user", value);
        editor.commit();

    }

    public  String getEmail(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("email_user", "");
        return status;
    }

    public void setUserName(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("user_name", value);
        editor.commit();

    }

    public  String getUserName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("user_name", "");
        return status;
    }

    public void setFirstName(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("first_name", value);
        editor.commit();

    }

    public  String getFirstName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("first_name", "");
        return status;
    }

    public void setLastName(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("last_name", value);
        editor.commit();

    }

    public  String getLastName(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("last_name", "");
        return status;
    }

    public void setPicture(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("picture", value);
        editor.commit();

    }

    public  String getPicture(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("picture", "");
        return status;
    }

    public void setCover(Context context, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("perbarindo", Context.MODE_PRIVATE).edit();
        editor.putString("cover", value);
        editor.commit();

    }

    public  String getCover(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("perbarindo",	Context.MODE_PRIVATE);
        String status = prefs.getString("cover", "");
        return status;
    }



}
