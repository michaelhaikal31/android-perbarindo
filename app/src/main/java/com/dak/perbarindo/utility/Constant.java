package com.dak.perbarindo.utility;

public class Constant {
    public static final String URL_CHECK_IN = "/v1/process/";
    public static final String URL_LOGIN = "/v1/auth/";
    public static final String URL_GET_COVER = "/v1/process/";

    public static final String URL_MENU = "http://service.perbarindo.org/v1/menu/main";

    public static final String URL_MENU_DETAIL = "/v1/menu/";
    public static final String URL_SUBMIT_DOCUMENT = "v1/process/submit_document";
    public static final String URL_SUBMIT_STATERKIT = "/v1/process/";
    public static final String email = "com.acs.perbarindo.LOGIN_DETAIL.EMAIL";
    public static final String first_name = "com.acs.perbarindo.LOGIN_DETAIL.FIRST_NAME";
    public static final String id_user = "com.acs.perbarindo.LOGIN_DETAIL.ID_USER";
    public static final String last_name = "com.acs.perbarindo.LOGIN_DETAIL.LAST_NAME";
    public static final String loading = "Please Wait";
    public static final String loadingBar = "Processing...";
    public static final String login_details = "com.acs.perbarindo.LOGIN_DETAIL";
    public static final String picture = "com.acs.perbarindo.LOGIN_DETAIL.PICTURE";
    public static final String status = "com.acs.perbarindo.LOGIN_DETAIL.STATUS";
    public static final String KEY = "p3rb@r1nd0";
    public static final String HEADER_VALUE = "PERBARINDO-API-156";


}
